import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Dimensions,
  WebView
} from 'react-native';

import {Bubbles} from 'react-native-loader';

export default class NewsDetails extends Component{
    constructor(props){
        super(props);
        this.state = {
            url :'',
            isLoaded : false
        }
    }

    componentDidMount(){
        this.loadInitialState().done();
    }

    loadInitialState = async() =>{
        this.setState({ url : this.props.url });
        setTimeout(() => {this.setState({isLoaded : true})}, 20000)
    }

    render(){
        if(this.state.isLoaded == false){
            return(
                <View style = {styles.container}>
          <Bubbles size = {10} color = "#C52B2B" />
        </View>
            )
        } else {
        return(
          <WebView
        source={{uri: this.state.url}}
        style={styles.webStyle}
      />
        );
        }
    }
}

const styles = StyleSheet.create({
    webStyle  : {
        marginTop : 20
    } 
})