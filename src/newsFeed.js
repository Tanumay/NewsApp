import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  FlatList,
  Dimensions
} from 'react-native';

import {
    Actions,
    Router,
    Scene,
    Drawer
} from 'react-native-router-flux';

import {Bubbles} from 'react-native-loader';

import {
  Card,
  CardTitle,
  CardContent,
  CardAction,
} from 'react-native-card-view';

export default class NewsFeed extends Component {
    constructor(props){
        super(props);
        this.state = {
            news : [],
            flag : 0,
            errorMessage : ''
        }
    }
    componentDidMount () {
        this.loadNews().done();
    }

    loadNews = async() => {
        var url = 'https://newsapi.org/v2/top-headlines?apiKey=54e969a24e53450f8de0acb811dab939&country=in';

        const newsData =await fetch(url);
        const newsDataJson = await newsData.json();
        if(newsDataJson.status === 'ok'){
            this.setState({ flag : 1});
            this.setState({ news : newsDataJson.articles});
        } else {
            this.setState({ flag : 2 });
            this.setState({ errorMessage : 'Something Went wrong! Please try again later.' })
        }
    }
    render(){
        if(this.state.flag === 0){
            return(
        <View style = {styles.container}>
          <Bubbles size = {10} color = "#C52B2B" />
        </View>
        )
        } else if(this.state.flag === 1)
            {
        return(
            <View style = {styles.container}>
                <FlatList
          data = {this.state.news}
          keyExtractor = {(x,i) => i}
          renderItem = {({item}) =>
          <Card styles = {{card: styles.cardStyle }}
          >
          <CardTitle><Text style = { styles.cardTitle }
          >
          {item.title}
          </Text>
          </CardTitle>
            <CardContent>
            <Text style = {styles.rowViewContainer}>
                  Soure :  {item.source.name} 
                   </Text>
                   <Text style = {styles.rowViewContainer} onPress = {() => this.newsDetails(item.url)}>
                  Details :  Click here 
                   </Text>
                   </CardContent>
                   </Card>
          }
          /> 
                </View>
        )
    } else {
        <View>
            <Text>{this.state.errorMessage}</Text>
            </View>
    }
}

newsDetails(url){
    // alert(url);
    Actions.newsDetails({url : url});
    
}

}

const styles = StyleSheet.create({
     container : {
        flex : 1,
        width : Dimensions.get('window').width,
        height : Dimensions.get('window').height,
        alignItems : 'center',
        justifyContent : 'center'
    },
    rowViewContainer : {
        fontSize : 15,
        color : '#FFF'  
      },
      cardStyle : {
        backgroundColor:'#C52B2B',
        width:340,
        marginTop:5
      },
      cardTitle : {
          fontSize : 15,
          fontWeight : 'bold', 
          color : "#FFF"
      }
})