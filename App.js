/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Dimensions,
  NetInfo
} from 'react-native';

import {
  Actions,
Router,
Scene,
Drawer
} from 'react-native-router-flux';

import NewsFeed from './src/newsFeed';
import NewsDetails from './src/newsDetails';

export default class App extends Component {
  componentDidMount(){
    this.checkInternet().done();
  }

  checkInternet = async() => {
     NetInfo.isConnected.fetch().then(isConncted => {
       console.log((isConncted ? 'online' : 'offline'));
     })
  }
  render() {
    return (
      <Router>
        <Scene key = 'root' style = {styles.container}>
          {/*<Scene key = 'home' component = {Home} hideNavBar = 'true' initial = 'true'/>*/}
          <Scene key = 'news' 
           component = {NewsFeed} 
           title = 'Daily News' 
           navigationBarStyle = { styles.navigationBarStyle }
           titleStyle = { styles.navigationTitle }
           initial = 'true'/>
           <scene
           key = 'newsDetails'
           component = {NewsDetails}
           title = 'News Details'
           navigationBarStyle = { styles.navigationBarStyle }
           titleStyle = { styles.navigationTitle }
           />
          </Scene>
          
        </Router>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  navigationBarStyle : {
    backgroundColor: '#ED0A0A'
  },
  navigationTitle : {
    color : "#FFF"
  }
});
